# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Icosaedro
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del icosaedro con texturas y sus diferentes transformaciones
# Es llamada por la clase Main.

from OpenGL.GLU import *
from Texturas import *

class Icosaedro:

    #Inicializador
   def __init__(self, gl):
        self.gl = gl
        self.t = Texturas()

    #Creación del icosaedro solido
   def crearIcosaedro(self,x,y,z,w,h,p,rx,ry,rz):
        self.gl.glPushMatrix()
        self.gl.glTranslatef(x, y, z)
        self.gl.glRotatef(rx, 1, 0, 0)
        self.gl.glRotatef(ry, 0, 1, 0)
        self.gl.glRotatef(rz, 0, 0, 1)
        self.gl.glScalef(w, h, p)

        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id)
        #dibujar triángulos que componen el Icosaedro
        #1
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f (-1.0,1.0,1.61803399)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f (1.0,1.0,1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f (0,-1.0,1.61803399)
        self.gl.glEnd()
        #2
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(1.0, 1.0, 1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, 1.61803399)
        self.gl.glEnd()
        #3
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(1.0, 1.0, 1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(1.618033999, 1.0, 0)
        self.gl.glEnd()
        #4
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(-1.0, 1.0, 1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, 1.61803399)
        self.gl.glEnd()
        #5
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(-1.0, 1.0, 1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(-1.618033999, 1.0, 0)
        self.gl.glEnd()
        # 6
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, -1.61803399)
        self.gl.glEnd()
        # 7
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, -1.61803399)
        self.gl.glEnd()
        # 8
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1,1.61)
        self.gl.glVertex3f(1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(1.618033999, 1.0, 0)
        self.gl.glEnd()
        # 9
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(-1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, -1.61803399)
        self.gl.glEnd()
        # 10
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(-1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(-1.618033999, 1.0, 0)
        self.gl.glEnd()
        #11
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f (-1.0,1.0,1.61803399)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f (1.0,1.0,1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f (0,1.61803399,0)
        self.gl.glEnd()
        #12
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(0, 1.61803399, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(1.0, 1.0, 1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(1.618033999, 1.0, 0)
        self.gl.glEnd()
        #13
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(0, 1.61803399, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(-1.0, 1.0, 1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(-1.618033999, 1.0, 0)
        self.gl.glEnd()
        # 14
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1, 1.61)
        self.gl.glVertex3f(-1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, 1.61803399, 0)
        self.gl.glEnd()
        # 15
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(0, 1.61803399, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(1.618033999, 1.0, 0)
        self.gl.glEnd()
        # 16
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(0, 1.61803399, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(-1.0, 1.0, -1.61803399)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(-1.618033999, 1.0, 0)
        self.gl.glEnd()
        #17
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(0, -1.61803399, 0)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, 1.61803399)
        self.gl.glEnd()
        #18
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(0, -1.61803399, 0)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, 1.61803399)
        self.gl.glEnd()
        # 19
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(0, -1.61803399, 0)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, -1.61803399)
        self.gl.glEnd()
        # 20
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(-1, 1,1.61)
        self.gl.glVertex3f(-1.61803399, -1.0, 0)
        self.gl.glTexCoord3f(1, 1, 1.61)
        self.gl.glVertex3f(0, -1.61803399, 0)
        self.gl.glTexCoord3f(0, -1,1.61)
        self.gl.glVertex3f(0, -1.0, -1.61803399)
        self.gl.glEnd()

        self.gl.glPopMatrix()