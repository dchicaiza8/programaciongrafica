# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Esfera
# Descripción: Una clase que tiene todos los componentes necesarios para la creación de la esfera con texturas y sus diferentes transformaciones
# Es llamada por la clase Main.

from OpenGL.GL import *
from OpenGL.GLU import *
from Texturas import *

class Esfera:
    # Inicializador
    def __init__(self, gl):
        self.gl = gl
        self.esfera = gluNewQuadric()
        self.t = Texturas()

    #Funcion para Crear una esfera
    def crearEsfera(self,tx,ty,tz,rx,ry,rz,r,g,b,rad,cor,pila):

        self.gl.glPushMatrix()
        self.gl.glTranslatef(tx,ty,tz)
        self.gl.glRotatef(rx,1,0,0)
        self.gl.glRotatef(ry, 0,1,0)
        self.gl.glRotatef(rz, 0,0,1)

        gluQuadricDrawStyle(self.esfera, GLU_FILL);
        gluQuadricTexture(self.esfera, True);
        gluQuadricNormals(self.esfera, GLU_SMOOTH);
        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id)
        gluSphere(self.esfera, rad,cor,pila)

        self.gl.glPopMatrix()

