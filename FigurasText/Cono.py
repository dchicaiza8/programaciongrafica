# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembre 2020
# Clase Cubo
# Descripción: Una clase que tiene todos los componentes necesarios para la creación del cono con texturas y sus diferentes transformaciones
# Es llamada por la clase main.

import math
import numpy as np
from Texturas import *
import OpenGL.GLUT.special as glt
from OpenGL.GLUT import *
class Cono:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl
        self.t = Texturas()

    #Función para crear un cono
    def crearCono(self,ratio,h,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):

        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        #self.gl.glColor3f(1, 0, 1)
        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id1)

        #Se define la base
        self.gl.glBegin(self.gl.GL_POLYGON)
        for i in np.arange(0.0, 10.0, 0.1):
            x = ratio * math.cos(i)
            y = ratio * math.sin(i)
            glTexCoord2f(x, y)
            self.gl.glVertex3f(x, y, 0)
        self.gl.glEnd()

        #Se define triángulos alrededor de la base que convergen a un punto
        self.gl.glBegin(self.gl.GL_TRIANGLE_FAN)

        glTexCoord2f(0, 0)
        self.gl.glVertex3f(0, 0, h)
        for i in np.arange(0.0, 10.0, 0.1):
            x = ratio * math.cos(i)
            y = ratio * math.sin(i)
            glTexCoord2f(x, y)
            self.gl.glVertex3f(x, y, 0)
        self.gl.glEnd()

        self.gl.glPopMatrix()