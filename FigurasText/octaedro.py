# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Octaedro
# Descripción: Esta clase permite la creación del octaedro con diferentes texturas en las caras y sus diferentes transformaciones
# Es llamada por la clase Main.

from Texturas import *

class Octaedro:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl
        self.t = Texturas()

    # Creación del octaedro solido

   def crearOctaedro(self,radio, sl, st, ex, ey, ez, tx, ty, tz, rx, ry, rz):
        self.gl.glPushMatrix()
        # Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glBindTexture(GL_TEXTURE_2D, self.t.texture_id)
        # Definimos vértices 1
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1, 0,0)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 2
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1, 0,0)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glTexCoord3f(0, 0,1)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 3
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1,0,0)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 4
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1,0,0)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 5
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1,0,0)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        # Definimos vértices 6
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1,0,0)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        # Definimos vértices 7
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1,0,0)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        # Definimos vértices 8
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glTexCoord3f(1,0,0)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glTexCoord3f(0,1,0)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glTexCoord3f(0,0,1)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()

        # Método para crear el octaedro de alambre, Wireframe

   def crearOctaedroWireFrame(self, r, g, b, s, radio, sl, st, ex, ey, ez, tx, ty, tz, rx, ry, rz):
        self.gl.glPushMatrix()
        # Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)  # grosor de linea

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 2
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 3
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 4
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, -2.000000)
        self.gl.glEnd()

        # Definimos vértices 5
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        # Definimos vértices 6
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        # Definimos vértices 7
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        # Definimos vértices 8
        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(2.000000, 0.000000, 0.000000)
        self.gl.glVertex3f(0.000000, -2.000000, 0.000000)
        self.gl.glVertex3f(0.000000, 0.000000, 2.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()