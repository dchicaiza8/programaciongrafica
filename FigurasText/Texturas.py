# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Primitivas OpenGl
# Descripción: Una clase que mantiene todos los componentes necesarios para aplicar texturas a las primitivas.
# Es llamada por las clases que crean cada una de las primitivas
from OpenGL.GL import *
from OpenGL.GLU import *

import pygame

class Texturas:

    def __init__(self):
        # Load the textures
        self.texture_surface = pygame.image.load("./Imagenes/textura1.jpg")
        # Retrieve the texture data
        self.texture_data = pygame.image.tostring(self.texture_surface, 'RGB', True)

        # Generate a texture id
        self.texture_id = glGenTextures(1)
        # Tell OpenGL we will be using this texture id for texture operations
        glBindTexture(GL_TEXTURE_2D, self.texture_id)

        # Tell OpenGL how to scale images
        #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT)
        #glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

        # Tell OpenGL that data is aligned to byte boundries
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

        # Get the dimensions of the image
        width, height = self.texture_surface.get_rect().size

        gluBuild2DMipmaps(GL_TEXTURE_2D,
                  3,
                  width,
                  height,
                  GL_RGB,
                  GL_UNSIGNED_BYTE,
                  self.texture_data)

        # Load the textures
        self.texture_surface1 = pygame.image.load("./Imagenes/textura3.jpg")
        # Retrieve the texture data
        self.texture_data1 = pygame.image.tostring(self.texture_surface1, 'RGB', True)

        # Generate a texture id
        self.texture_id1 = glGenTextures(1)
        # Tell OpenGL we will be using this texture id for texture operations
        glBindTexture(GL_TEXTURE_2D, self.texture_id1)

        # Tell OpenGL how to scale images
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

        # Tell OpenGL that data is aligned to byte boundries
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)

        # Get the dimensions of the image
        width1, height1 = self.texture_surface1.get_rect().size

        gluBuild2DMipmaps(GL_TEXTURE_2D,
                          3,
                          width1,
                          height1,
                          GL_RGB,
                          GL_UNSIGNED_BYTE,
                          self.texture_data1)


