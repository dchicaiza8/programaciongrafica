# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Diciembree 2020
# Clase prisma
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del prisma y sus diferentes transformaciones
# Es llamada por la clase main.

import math
class Prisma:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl
    #metodo para crear el prisma solido
    def crearPrisma(self, x,y,z,w,h,p,rx,ry,rz,r,g,b):
        self.gl.glPushMatrix()
        self.gl.glTranslatef(x, y, z)
        self.gl.glRotatef(rx, 1, 0, 0)
        self.gl.glRotatef(ry, 0, 1, 0)
        self.gl.glRotatef(rz, 0, 0, 1)
        self.gl.glScalef(w, h, p)

        #dibujar triángulos que componen el prisma
        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (1.0,1.0,-1.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (-1.0,1.0,0.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (1.0,1.0,1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (-1.0,-1.0,0.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (1.0,-1.0,-1.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (1.0,-1.0,1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (1.0,1.0,1.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (-1.0,-1.0,0.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (1.0,-1.0,1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (1.0,1.0,-1.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (1.0,-1.0,1.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (1.0,-1.0,-1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (-1.0,1.0,0.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (1.0,-1.0,-1.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (-1.0,-1.0,0.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (1.0,1.0,1.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (-1.0,1.0,0.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (-1.0,-1.0,0.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (1.0,1.0,-1.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (1.0,1.0,1.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (1.0,-1.0,1.0)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_TRIANGLES)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f (-1.0,1.0,0.0)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f (1.0,1.0,-1.0)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f (1.0,-1.0,-1.0)
        self.gl.glEnd()
        self.gl.glPopMatrix()