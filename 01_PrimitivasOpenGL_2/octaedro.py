# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Octaedro
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del octaedro y sus diferentes transformaciones
# Es llamada por la clase Main.

from OpenGL.GLU import *

class Octaedro:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl

    #Creación del octaedro solido
   def crearOctaedro(self,r,g,b,radio,sl, st, ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
        #Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       octaedro = gluNewQuadric()
       self.gl.glColor3f(r, g, b)
       gluSphere(octaedro, radio, sl,st)

       self.gl.glPopMatrix()

#Método para crear el octaedro de alambre, Wireframe
   def crearOctaedroWireFrame(self, r, g, b, s ,radio, sl, st, ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
       # Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       self.gl.glColor3f(r, g, b)
       self.gl.glLineWidth(s)#grosor de linea
       octaedro = gluNewQuadric()
       gluQuadricDrawStyle(octaedro, GLU_LINE);#para graficar cilindro tipo wireframe o alambre
       self.gl.glColor3f(r, g, b)
       gluSphere(octaedro, radio, sl, st)

       self.gl.glPopMatrix()

