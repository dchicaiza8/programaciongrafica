# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Esfera
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación de las esfera y sus diferentes transformaciones
# Es llamada por la clase Main.

from OpenGL.GL import *
from OpenGL.GLU import *

class Esfera:
    # Inicializador
    def __init__(self, gl):
        self.gl = gl

    #Funcion para Crear una esfera
    def crearEsfera(self,tx,ty,tz,rx,ry,rz,r,g,b,rad,cor,pila):

        self.gl.glPushMatrix()
        self.gl.glTranslatef(tx,ty,tz)
        self.gl.glRotatef(rx,1,0,0)
        self.gl.glRotatef(ry, 0,1,0)
        self.gl.glRotatef(rz, 0,0,1)
        self.gl.glColor3f(r,g,b)

        #gluSphere (POINTER (GLUquadric) (quad), GLdouble (radio), GLint (cortes), GLint (pilas))
        gluSphere(gluNewQuadric(), rad,cor,pila)
        self.gl.glPopMatrix()
#Funcion para Crear una esfera
    def crearEsferaWireframe(self,tx,ty,tz,rx,ry,rz,r,g,b,l,rad,cor,pila):

        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glTranslatef(tx,ty,tz)
        self.gl.glRotatef(rx,1,0,0)
        self.gl.glRotatef(ry, 0,1,0)
        self.gl.glRotatef(rz, 0,0,1)

        self.gl.glColor3f(r,g,b)
        #Define el Grosor de linea
        self.gl.glLineWidth(l)
        esfera = gluNewQuadric()
        #Grafica Esfera tipo alambre o WireFrame
        gluQuadricDrawStyle(esfera, GLU_LINE);
        self.gl.glColor3f(r, g, b)
        gluSphere(esfera, rad, cor, pila)

        self.gl.glPopMatrix()

