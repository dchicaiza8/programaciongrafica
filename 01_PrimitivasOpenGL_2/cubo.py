# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Cubo
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación del cubo y sus diferentes transformaciones
# Es llamada por la clase main.

import math
class Cubo:
    #Inicializador
    def __init__(self,gl):
        self.gl = gl

    #Función para crear un cubo
    def crearCubo(self,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)
        #Definimos vértices
        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUADS)
        self.gl.glColor3f(r, 0, 0)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glColor3f(0, g, 0)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glColor3f(0, 0, b)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()

    def crearCuboWireFrame(self,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):

        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glVertex3f(-1.000000, -1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, 1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, 1.000000)
        self.gl.glEnd()


        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        self.gl.glVertex3f(-1.000000, -1.000000, -1.000000)
        self.gl.glVertex3f(-1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, 1.000000, -1.000000)
        self.gl.glVertex3f(1.000000, -1.000000, -1.000000)
        self.gl.glEnd()

        self.gl.glPopMatrix()