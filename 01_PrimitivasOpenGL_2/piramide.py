# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Noviembre 2020
# Clase Piramide
# Descripción: Una clase que mantiene todos los componentes necesarios para la creación de la pirámide y sus diferentes transformaciones
# Es llamada por la clase Main.
class Piramide:
    #Inicializador
   def __init__(self, gl):
        self.gl = gl

    #Creación de la pirámide con sus diferentes vértices
   def crearPiramide(self,r,g,b,lado,ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()
        #Transformaciones
       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)

       # Frente
       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r,0,0)
       self.gl.glVertex(0, 0, 0)
       self.gl.glColor3f(0, g, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glColor3f(0, 0, b)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glEnd()

       # Lateral Derecho

       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r, 0, 0)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glColor3f(0, b, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glColor3f(0, 0, g)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       # Lateral Derecho

       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r, 0, 0)
       self.gl.glVertex(0, 0, 0)
       self.gl.glColor3f(0, b, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glColor3f(0, 0, g)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       # Lateral Derecho
       self.gl.glBegin(self.gl.GL_TRIANGLES)
       self.gl.glColor3f(r, 0, 0)
       self.gl.glVertex(0, 0, lado)
       self.gl.glColor3f(0, b, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glColor3f(0, 0, g)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       self.gl.glBegin(self.gl.GL_QUADS)
       self.gl.glColor3f(r, 0, 0)
       self.gl.glVertex(0, 0, 0)
       self.gl.glColor3f(0, b, 0)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glColor3f(0, 0, g)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glColor3f(r, 0, 0)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       self.gl.glPopMatrix()
#Método para crear la pirámide de alambre, Wireframe
   def crearPiramideWireFrame(self, r, g, b, s,lado,ex,ey,ez,tx,ty,tz,rx,ry,rz):
       self.gl.glPushMatrix()

       self.gl.glScalef(ex, ey, ez)
       self.gl.glTranslate(tx, ty, tz)
       self.gl.glRotate(rx, 1, 0, 0)
       self.gl.glRotate(ry, 0, 1, 0)
       self.gl.glRotate(rz, 0, 0, 1)


       self.gl.glColor3f(r, g, b)
       self.gl.glLineWidth(s)

       # Frente
       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glEnd()

       # Lateral Derecho

       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       # Lateral Derecho

       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, 0)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       # Lateral Derecho
       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, lado)
       self.gl.glVertex(lado / 2, lado, lado / 2)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glEnd()

       self.gl.glBegin(self.gl.GL_LINE_LOOP)
       self.gl.glVertex(0, 0, 0)
       self.gl.glVertex(lado, 0, 0)
       self.gl.glVertex(lado, 0, lado)
       self.gl.glVertex(0, 0, lado)
       self.gl.glEnd()

       self.gl.glPopMatrix()